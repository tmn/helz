/*! The overall program config of all notifiers.
*/

#[deny(missing_docs)]
pub mod config;
#[deny(missing_docs)]
pub mod discord;
#[deny(missing_docs)]
pub mod email;
#[deny(missing_docs)]
pub mod opsgenie;
#[deny(missing_docs)]
pub mod slack;
#[deny(missing_docs)]
pub mod stdout;
#[deny(missing_docs)]
pub mod traits;
#[deny(missing_docs)]
pub mod webex;
