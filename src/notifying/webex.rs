/*! Webex notifier
*/

use async_trait::async_trait;
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use serde_json::json;
use serde_json::ser::to_string;
use std::convert::From;
use url::Url;

use crate::config::traits::Example;
use crate::notifying::traits::{error_message_detailed, ok_message, Notifier};
use crate::polling::config::HelzCheckStatus;
use crate::types::string_secret::Secret;

/// Configuration for the WebexNotifier
///
/// The values can be configured in TOML as follows:
/// ```
/// # use libhelz::notifying::webex::WebexNotifierConfig;
/// # let conf: WebexNotifierConfig = toml::from_str(r#"
/// api_token = "token"
/// room_id = "room id"
/// # "#).expect("example configuration should work");
/// ```
///
/// Full example:
/// ```
/// # use libhelz::config::main::configure;
/// # let config = configure(r#"
/// [[polling]]
/// [polling.notifier]
/// interval = "5s"
/// [polling.notifier.webex]
/// api_token = "token"
/// room_id = "room id"
/// # "#).expect("example configuration should work");
/// ```
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct WebexNotifierConfig {
    /// The API token to use to communicate with the Webex API.
    pub api_token: Secret,
    /// Webex Room ID can be fetched using the Webex REST API
    /// with a GET /v1/rooms
    ///
    /// Then you have to match the room name with the one you want
    /// notifications to go to.
    ///
    /// Webex API docs: <https://developer.webex.com/docs/api/v1/rooms/list-rooms>
    pub room_id: String,
}

#[derive(Clone)]
/// The runtime struct of a Webex notifier.
pub struct WebexNotifier {
    api_token: String,
    room_id: String,
}

#[async_trait]
impl Notifier for WebexNotifier {
    async fn notify(&self, errors: &[HelzCheckStatus], whoami: &str) {
        trace!("webex notify err");
        let url = Url::parse("https://webexapis.com/v1/messages")
            .expect("failed to parse statically configured URL");
        let client = reqwest::Client::new();

        let message = error_message_detailed(errors, whoami);

        let body = json!({ "roomId": self.room_id, "markdown": message })
            .as_object()
            .expect("failed to convert notification data to json for webex webhook")
            .to_owned();
        let body_str =
            to_string(&body).expect("failed to convert webex webhook json data to string");

        let resp = match client
            .post(url)
            .header("Authorization", format!("Bearer {}", self.api_token))
            .header("Content-Type", "application/json")
            .body(body_str)
            .send()
            .await
        {
            Ok(resp) => {
                debug!("received response from webex api");
                resp
            }
            Err(err) => {
                error!("failed to send webex notify: {}", err);
                return;
            }
        };

        match resp.status() {
            StatusCode::OK => trace!("got 200 OK from webex API"),
            _ => {
                error!(
                    "HTTP response from webex was not 200 OK as expected, was {}",
                    resp.status()
                );
                match resp.text().await {
                    Ok(data) => debug!("response body for HTTP error: {}", data),
                    Err(err) => debug!("failed to parse response body for HTTP error: {}", err),
                };
                return;
            }
        };
    }

    async fn ok(&self, errors: &[HelzCheckStatus], whoami: &str) {
        trace!("webex notify ok");
        let url = Url::parse("https://webexapis.com/v1/messages")
            .expect("failed to parse statically configured URL");
        let client = reqwest::Client::new();

        let message = ok_message(errors, whoami);

        let body = json!({ "roomId": self.room_id, "markdown": message })
            .as_object()
            .expect("failed to convert notification data to json for webex webhook")
            .to_owned();
        let body_str =
            to_string(&body).expect("failed to convert webex webhook json data to string");

        match client
            .post(url)
            .header("Authorization", format!("Bearer {}", self.api_token))
            .header("Content-Type", "application/json")
            .body(body_str)
            .send()
            .await
        {
            Ok(_) => debug!("successfully sent webex notify"),
            Err(err) => error!("failed to send webex notify: {}", err),
        };
    }
}

impl From<WebexNotifierConfig> for WebexNotifier {
    fn from(conf: WebexNotifierConfig) -> Self {
        WebexNotifier {
            api_token: conf.api_token.expose(),
            room_id: conf.room_id,
        }
    }
}

impl Example for WebexNotifierConfig {
    fn example() -> Self {
        Self {
            room_id: "foo".to_owned(),
            api_token: Secret("bar".to_owned()),
        }
    }
}
