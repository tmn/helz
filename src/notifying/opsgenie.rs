/*! Opsgenie notifier
*/

use std::collections::HashMap;

use async_trait::async_trait;
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use serde_json::json;
use serde_json::ser::to_string;

use crate::notifying::traits::{error_message_detailed, ok_message, Notifier};
use crate::polling::config::HelzCheckStatus;
use crate::types::string_secret::Secret;

macro_rules! API_URL {
    () => {
        "https://api.opsgenie.com/v2"
    };
}
const API_URL_ALERTS: &str = concat!(API_URL!(), "/alerts");

/// Configuration for OpsGenie notifier.
///
/// The values can be configured in TOML as follows:
/// ```
/// # use libhelz::notifying::opsgenie::OpsGenieConfig;
/// # let conf: OpsGenieConfig = toml::from_str(r#"
/// api_key = "foo-bar"
/// # "#).expect("example configuration should work");
/// ```
///
/// And it is possible to provide more metadata to the alerts
/// in the `alert` table:
/// ```
/// # use libhelz::notifying::opsgenie::OpsGenieConfig;
/// # let conf: OpsGenieConfig = toml::from_str(r#"
/// api_key = "foo-bar"
/// [alert]
/// source = "helz"
/// tags = ["testing"]
/// # "#).expect("example configuration should work");
/// ```
///
/// Full example:
/// ```
/// # use libhelz::config::main::configure;
/// # let config = configure(r#"
/// [[polling]]
/// [polling.notifier]
/// interval = "5s"
/// [polling.notifier.opsgenie]
/// api_key = "foo-bar"
/// [polling.notifier.alert]
/// tags = ["testing"]
/// # "#).expect("example configuration should work");
/// ```
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct OpsGenieConfig {
    /// API Key for OpsGenie.
    /// Refer to [OpsGenie API Documentation](https://support.atlassian.com/opsgenie/docs/create-a-default-api-integration/)
    /// on how to create one.
    pub api_key: Secret,
    /// Optional data to enrich the alert with,
    /// in addition to the 'message', 'alias' and 'description'
    /// (generated from the health check).
    pub alert: Option<OpsGenieAlertConfig>,
}

/// Data to enrich an OpsGenie alert with.
/// All fields are optional.
/// Refer to the [OpsGenie documentation](https://docs.opsgenie.com/docs/alert-api#create-alert) for
/// up-to-date description and usage instructions for each field.
#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct OpsGenieAlertConfig {
    responders: Option<String>,
    #[serde(rename = "visibleTo")]
    visible_to: Option<String>,
    actions: Option<String>,
    tags: Option<Vec<String>>,
    details: Option<HashMap<String, String>>,
    entity: Option<String>,
    source: Option<String>,
    /// Must be either P1, P2, P3, P4 or P5. Defaults to P3 if left empty.
    priority: Option<String>,
    user: Option<String>,
    note: Option<String>,
}

/// Runtime struct for OpsGenie notifier.
pub struct OpsGenie {
    client: reqwest::Client,
    alert_details: OpsGenieAlertConfig,
}

#[derive(Deserialize, Serialize)]
struct OpsGenieAlert<'a> {
    message: &'a str,
    alias: &'a str,
    description: &'a str,
}

#[derive(Deserialize, Serialize)]
struct OpsGenieOk<'a> {
    note: &'a str,
}

impl From<OpsGenieConfig> for OpsGenie {
    fn from(conf: OpsGenieConfig) -> Self {
        let mut headers = reqwest::header::HeaderMap::new();
        headers.insert(
            "Content-Type",
            reqwest::header::HeaderValue::from_static("application/json"),
        );

        let auth_header_string = format!("GenieKey {}", conf.api_key.expose());
        let mut auth_header_value = reqwest::header::HeaderValue::from_str(&auth_header_string)
            .expect("failed to create opsgenie auth token header");
        auth_header_value.set_sensitive(true);
        headers.insert("Authorization", auth_header_value);

        let client = reqwest::Client::builder()
            .default_headers(headers)
            .user_agent(format!("helz {}", clap::crate_version!()))
            .build()
            .expect("failed to build client for opsgenie notifications");

        Self {
            client,
            alert_details: conf.alert.unwrap_or_default(),
        }
    }
}

#[async_trait]
impl Notifier for OpsGenie {
    async fn ok(&self, errors: &[HelzCheckStatus], whoami: &str) {
        let dedup_key = &self.dedup_key(errors);
        let body = to_string(
            json!(OpsGenieOk {
                note: &ok_message(errors, whoami)
            })
            .as_object()
            .expect("failed to create json body from opsgenie ok notify"),
        )
        .expect("failed to convert opsgenie json to string for http request");

        let resp = match self
            .client
            .post(format!(
                "{}/{}/close?identifierType=alias",
                API_URL_ALERTS, dedup_key
            ))
            .body(body)
            .send()
            .await
        {
            Ok(resp) => {
                trace!("received response from opsgenie api");
                resp
            }
            Err(err) => {
                error!("failed to send opsgenie alert: {}", err);
                return;
            }
        };

        match resp.status() {
            StatusCode::ACCEPTED => trace!("got 202 Accepted from opsgenie API"),
            _ => {
                error!(
                    "HTTP response from opsgenie was not 202 Accepted as expected, was {}",
                    resp.status()
                );
                match resp.text().await {
                    Ok(data) => debug!("response body for HTTP error: {}", data),
                    Err(err) => debug!("failed to parse response body for HTTP error: {}", err),
                };
                return;
            }
        }
    }

    async fn notify(&self, errors: &[HelzCheckStatus], whoami: &str) {
        let first_error = &errors[0];
        let alert = OpsGenieAlert {
            message: &format!("{} - {}", first_error.name, first_error.response),
            alias: &self.dedup_key(errors),
            description: &error_message_detailed(errors, whoami),
        };
        let mut body = json!(alert)
            .as_object()
            .expect("failed to convert notification data to json for opsgenie webhook")
            .to_owned();
        let mut ext = json!(self.alert_details)
            .as_object()
            .expect("failed to convert notification detailed data")
            .to_owned();
        body.append(&mut ext);

        let body_str =
            to_string(&body).expect("failed to convert opsgenie webhook json data to string");

        debug!(
            "sending alert to opsgenie with alias/dedup '{}':\n{}",
            alert.alias, alert.message
        );

        let resp = match self.client.post(API_URL_ALERTS).body(body_str).send().await {
            Ok(resp) => {
                trace!("received response from opsgenie api");
                resp
            }
            Err(err) => {
                error!("failed to send opsgenie alert: {}", err);
                return;
            }
        };

        match resp.status() {
            StatusCode::ACCEPTED => trace!("got 202 Accepted from opsgenie API"),
            _ => {
                error!(
                    "HTTP response from opsgenie was not 202 Accepted as expected, was {}",
                    resp.status()
                );
                match resp.text().await {
                    Ok(data) => debug!("response body for HTTP error: {}", data),
                    Err(err) => debug!("failed to parse response body for HTTP error: {}", err),
                };
                return;
            }
        };
    }
}

impl OpsGenie {
    fn dedup_key(&self, errors: &[HelzCheckStatus]) -> String {
        let first_error = &errors[0];
        format!(
            "{}-{}",
            first_error.name,
            first_error.human_time.to_rfc3339()
        )
    }
}
