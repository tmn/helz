/*! Email notifier

The good old email. Do people still use this?
*/

use async_trait::async_trait;
use lettre::message::Mailbox;
use lettre::{Message, SmtpTransport, Transport};
use serde::{Deserialize, Serialize};
use std::cmp::{max, min};
use std::thread;
use url::Url;

use crate::notifying::traits::Notifier;
use crate::polling::config::HelzCheckStatus;

#[derive(Clone, Debug, Deserialize, Serialize)]
/// The configuration for an email notifier.
pub struct EmailNotifierConfig {
    /// The subject of an error email.
    pub error_subject: EmailSubject,
    /// The subject of an OK email.
    pub ok_subject: EmailSubject,
    /// The email address to send the email to.
    pub to: Mailbox,
    /// The email address to send the email fron.
    pub from: Mailbox,
    /// The mail server to use.
    pub mailhost: Url,
    /// Port of the mail server to use.
    pub mailport: u16,
    /// Optionally define the amount of earlier errors to include in the email.
    pub list_previous_errors: Option<u64>,
}

type EmailSubject = String;

#[derive(Clone)]
/// The runtime struct of the email notifier.
pub struct EmailNotifier {
    error_subject: EmailSubject,
    ok_subject: EmailSubject,
    to: Mailbox,
    from: Mailbox,
    mailhost: Url,
    mailport: u16,
    list_previous_errors: u64,
}

impl From<EmailNotifierConfig> for EmailNotifier {
    fn from(conf: EmailNotifierConfig) -> Self {
        let list_previous_errors = conf.list_previous_errors.unwrap_or(0);
        EmailNotifier {
            error_subject: conf.error_subject,
            ok_subject: conf.ok_subject,
            to: conf.to,
            from: conf.from,
            mailhost: conf.mailhost,
            mailport: conf.mailport,
            list_previous_errors,
        }
    }
}

#[async_trait]
impl Notifier for EmailNotifier {
    async fn notify(&self, errors: &[HelzCheckStatus], whoami: &str) {
        let first_error = &errors[0];
        let last_error = &errors[errors.len() - 1];

        trace!("email notify from '{}' to '{}'", self.from, self.to);

        let backlog_num = min(self.list_previous_errors as usize, errors.len());
        let mut backlog = format!("Last {} checks:\n", backlog_num);
        let backlog_start = max(0, errors.len() as i32 - backlog_num as i32) as usize;
        for event in &errors[backlog_start..errors.len()] {
            backlog = format!("{}\n{:?} {}", backlog, event.human_time, event.response);
        }

        let body = format!(
            r#"
The service {} has been detected as down.

First occurence: {:?} ({}s ago)
Last occurence: {:?} ({}s ago)

{}

-- Regards, Helz @ {}
"#,
            first_error.name,
            first_error.human_time,
            first_error.time.elapsed().as_secs(),
            last_error.human_time,
            last_error.time.elapsed().as_secs(),
            backlog,
            whoami,
        );

        let email = match Message::builder()
            .from(self.from.to_owned())
            .to(self.to.to_owned())
            .subject(self.error_subject.to_owned())
            .body(body)
        {
            Ok(email) => email,
            Err(err) => {
                error!("failed to construct email: {}", err);
                return;
            }
        };

        // ToDo: create this during init and reuse it
        let mailer = SmtpTransport::builder_dangerous(self.mailhost.as_str())
            .port(self.mailport)
            .build();

        thread::spawn(move || {
            match mailer.send(&email) {
                Ok(_) => trace!("sent email ok"),
                Err(err) => error!("sending email failed: {}", err),
            };
        });
    }

    async fn ok(&self, errors: &[HelzCheckStatus], whoami: &str) {
        let first_error = &errors[0];
        let last_error = &errors[errors.len() - 1];
        trace!("email notify from '{}' to '{}'", self.from, self.to);

        let backlog_num = min(self.list_previous_errors as usize, errors.len());
        let mut backlog = format!("Last {} checks", backlog_num);
        let backlog_start = max(0, errors.len() as i32 - backlog_num as i32 - 1) as usize;
        for event in &errors[backlog_start..errors.len()] {
            backlog = format!("{}\n{:?} {}", backlog, event.human_time, event.response);
        }

        let body = format!(
            r#"
The service {} has been detected as up.

First occurence: {:?} ({}s ago)
Last occurence: {:?} ({}s ago)

{}

-- Regards, Helz @ {}
"#,
            first_error.name,
            first_error.human_time,
            first_error.time.elapsed().as_secs(),
            last_error.human_time,
            last_error.time.elapsed().as_secs(),
            backlog,
            whoami,
        );

        let email = match Message::builder()
            .from(self.from.to_owned())
            .to(self.to.to_owned())
            .subject(self.ok_subject.to_owned())
            .body(body)
        {
            Ok(email) => email,
            Err(err) => {
                error!("failed to construct email: {}", err);
                return;
            }
        };

        // ToDo: create this during init and reuse it
        let mailer = SmtpTransport::builder_dangerous(self.mailhost.as_str())
            .port(self.mailport)
            .build();

        thread::spawn(move || {
            match mailer.send(&email) {
                Ok(_) => trace!("sent email ok"),
                Err(err) => error!("sending email failed: {}", err),
            };
        });
    }
}
