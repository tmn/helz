/*! Slack notifier
*/

use async_trait::async_trait;
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use serde_json::json;
use serde_json::ser::to_string;
use std::convert::From;
use url::Url;

use crate::config::traits::Example;
use crate::notifying::traits::{
    error_message_detailed, ok_message, unique_errors_output, Notifier,
};
use crate::polling::config::HelzCheckStatus;

/// Configuration for the SlackNotifier
///
/// The values can be configured in TOML as follows:
/// ```
/// # use libhelz::notifying::slack::SlackConfig;
/// # let conf: SlackConfig = toml::from_str(r#"
/// webhook_url = "https://hooks.slack.com/services/FOO/BAR/BAZ"
/// # "#).expect("example configuration should work");
/// ```
///
/// Full example:
/// ```
/// # use libhelz::config::main::configure;
/// # let config = configure(r#"
/// [[polling]]
/// [polling.notifier]
/// interval = "5s"
/// [polling.notifier.slack]
/// webhook_url = "https://hooks.slack.com/services/FOO/BAR/BAZ"
/// enable_slack_date_formatting = true
/// # "#).expect("example configuration should work");
/// ```
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SlackConfig {
    /// Slack Webhook URL. Works for both apps and "outdated" webhooks.
    pub webhook_url: Url,
    /// Enable Slack date and time formatting.
    /// This removes the current RFC3339-style format
    /// for dates and times used, and makes the date and
    /// times display in the time zone of the viewer.
    /// Read more: https://api.slack.com/reference/surfaces/formatting#date-formatting
    pub enable_slack_date_formatting: Option<bool>,
}

#[derive(Clone)]
/// The runtime struct of the Slack notifier.
pub struct Slack {
    webhook_url: Url,
    enable_slack_date_formatting: bool,
    client: reqwest::Client,
}

#[async_trait]
impl Notifier for Slack {
    async fn notify(&self, errors: &[HelzCheckStatus], whoami: &str) {
        trace!("slack notify err");
        let message = if self.enable_slack_date_formatting {
            let first_error = &errors[0];
            let last_error = &errors[errors.len() - 1];
            let unique_error_text = unique_errors_output(errors);
            format!(
                r#"
The service `{}` has been detected as **down** by `{}` ❌.

First occurence: <!date^{}^{{date_num}} {{time_secs}}|{}> ({}s ago)
Last occurence: <!date^{}^{{date_num}} {{time_secs}}|{}> ({}s ago)

Unique errors:
{}
"#,
                first_error.name,
                whoami,
                first_error.human_time.timestamp(),
                first_error
                    .human_time
                    .to_rfc3339_opts(chrono::SecondsFormat::Secs, true),
                first_error.time.elapsed().as_secs(),
                last_error.human_time.timestamp(),
                last_error
                    .human_time
                    .to_rfc3339_opts(chrono::SecondsFormat::Secs, true),
                last_error.time.elapsed().as_secs(),
                unique_error_text,
            )
        } else {
            error_message_detailed(errors, whoami)
        };

        let body = json!({ "text": message, "type": "mrkdwn" })
            .as_object()
            .expect("failed to convert notification data to json for slack webhook")
            .to_owned();
        let body_str =
            to_string(&body).expect("failed to convert slack webhook json data to string");

        let resp = match self
            .client
            .post(self.webhook_url.to_owned())
            .header("Content-Type", "application/json")
            .body(body_str)
            .send()
            .await
        {
            Ok(resp) => {
                trace!("received response from slack api");
                resp
            }
            Err(err) => {
                error!("failed to send slack notify: {}", err);
                return;
            }
        };

        match resp.status() {
            StatusCode::OK => trace!("got 200 OK from slack API"),
            _ => {
                error!(
                    "HTTP response from slack was not 200 OK as expected, was {}",
                    resp.status()
                );
                match resp.text().await {
                    Ok(data) => debug!("response body for HTTP error: {}", data),
                    Err(err) => debug!("failed to parse response body for HTTP error: {}", err),
                };
                return;
            }
        };
    }

    async fn ok(&self, errors: &[HelzCheckStatus], whoami: &str) {
        trace!("slack notify ok");

        let message = if self.enable_slack_date_formatting {
            let first_error = &errors[0];
            let last_error = &errors[errors.len() - 1];
            let unique_error_text = unique_errors_output(errors);
            format!(
                r#"
The service `{}` has been detected as **up** by `{}` ✅.

First occurence: {:?} ({}s ago)
Last occurence: {:?} ({}s ago)

Errors seen:
{}
"#,
                first_error.name,
                whoami,
                first_error.human_time.timestamp(),
                first_error.time.elapsed().as_secs(),
                last_error.human_time.timestamp(),
                last_error.time.elapsed().as_secs(),
                unique_error_text,
            )
        } else {
            ok_message(errors, whoami)
        };

        let body = json!({ "text": message, "type": "mrkdwn" })
            .as_object()
            .expect("failed to convert notification data to json for slack webhook")
            .to_owned();
        let body_str =
            to_string(&body).expect("failed to convert slack webhook json data to string");

        match self
            .client
            .post(self.webhook_url.to_owned())
            .header("Content-Type", "application/json")
            .body(body_str)
            .send()
            .await
        {
            Ok(_) => trace!("successfully sent slack notify"),
            Err(err) => error!("failed to send slack notify: {}", err),
        };
    }
}

impl From<SlackConfig> for Slack {
    fn from(conf: SlackConfig) -> Self {
        let client = reqwest::Client::new();
        Slack {
            webhook_url: conf.webhook_url,
            enable_slack_date_formatting: conf.enable_slack_date_formatting.unwrap_or(false),
            client,
        }
    }
}

impl Example for SlackConfig {
    fn example() -> Self {
        Self {
            webhook_url: Url::parse("https://hooks.slack.com/services/FOO/BAR/BAZ")
                .expect("failed to parse statically configured URL"),
            enable_slack_date_formatting: Some(false),
        }
    }
}
