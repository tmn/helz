/*!
A health checker program.

This is a very simple binary target for running helz,
the documentation for how to use the various
configuration parameters can be found in the modules of
[libhelz](../libhelz).
*/

extern crate clap;
extern crate serde_json;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate pretty_env_logger;

use clap::{Parser, Subcommand};
use futures::StreamExt;
use log::LevelFilter;

use libhelz::config::file::read_config_file;
use libhelz::config::main::Config;
use libhelz::config::traits::Example;
use libhelz::polling::config::Polling;

/// The unsuccessful variants this program can
/// exit with.
#[derive(Debug)]
enum ProgramError {
    ConfigError,
}

#[derive(Parser, Debug)]
#[clap(author, version, about)]
struct Cli {
    /// Path to TOML configuration file
    #[clap(short = 'f', long = "config", default_value = "conf.toml")]
    config: String,

    /// Log level to display (and above).
    /// This can be configured more in detail using the
    /// environment variable RUST_LOG, to specify which
    /// *crates* to apply a specific filter to.
    /// In the packaged versions of Helz, logging from the
    /// AWS SDK is significantly reduced by setting
    /// RUST_LOG=aws_smithy_http_tower=warn in the default
    /// unit file.
    #[clap(short, long)]
    loglevel: Option<String>,

    /// Verify configuration and print it to stdout
    /// Use the 'check' program instead, through
    /// `helz check`.
    #[clap(short, long)]
    #[deprecated]
    check: bool,

    #[clap(subcommand)]
    programs: Option<CliPrograms>,
}

#[derive(Subcommand, Debug)]
#[clap(author, version, about)]
enum CliPrograms {
    /// Show example configuration
    #[clap(alias = "config")]
    ShowConfig,

    /// Check configuration
    #[clap(alias = "check")]
    CheckConfig,
}

struct Helz {
    config_path: String,
    config: Option<Config>,
    config_v: i32,
    pollers: Option<Vec<Polling>>,
    signals: tokio::sync::watch::Receiver<i32>,
}

#[tokio::main]
async fn main() -> Result<(), ProgramError> {
    let cli = Cli::parse();

    init_logger(&cli);

    if let Some(program) = &cli.programs {
        match program {
            CliPrograms::CheckConfig => {
                let config = parse_config(&cli.config)?;
                println!(
                    "{}",
                    toml::to_string(&config)
                        .expect("expecting parsed config to be able to serialize")
                );
                return Ok(());
            }
            CliPrograms::ShowConfig => {
                // generate an example configuration to print to stdout
                let config = libhelz::config::main::Config::example();
                println!(
                    "{}",
                    toml::to_string(&config).expect("expecting example config to work")
                );
                return Ok(());
            }
        }
    }

    #[allow(deprecated)]
    if cli.check {
        let config = parse_config(&cli.config)?;
        warn!("the use of the check flag has been deprecated, use the check program instead");
        println!(
            "{}",
            toml::to_string(&config).expect("expecting parsed config to be able to serialize")
        );
        return Ok(());
    }

    let mut config_v = 0;

    let (tx, rx) = tokio::sync::watch::channel::<i32>(10);
    let signals = signal_hook_tokio::Signals::new([signal_hook::consts::signal::SIGHUP]).unwrap();
    tokio::spawn(async move {
        signals_watcher(signals, tx).await;
    });

    let mut previous_config: Option<Config> = None;

    loop {
        trace!("confing helz from main");

        let helz = Helz {
            config_path: cli.config.to_owned(),
            config: previous_config,
            config_v,
            pollers: None,
            signals: rx.to_owned(),
        };

        config_v += 1;
        previous_config = Some(parse_config(&cli.config.to_owned()).unwrap());

        trace!("starting helz from main");
        helz.start().await?
    }
}

async fn signals_watcher(
    mut signals: signal_hook_tokio::Signals,
    tx: tokio::sync::watch::Sender<i32>,
) {
    debug!("starting watching for unix signals");
    let mut v = 0;
    loop {
        while let Some(signal) = signals.next().await {
            match signal {
                signal_hook::consts::signal::SIGHUP => {
                    debug!("sighup on config v{}, bumping to v{}", v, v + 1);
                    v += 1;
                    tx.send_replace(v);
                }
                _ => {
                    warn!("unhandled signal");
                }
            }
        }
    }
}

impl Helz {
    async fn start(mut self) -> Result<(), ProgramError> {
        self.configure().await?;

        info!("starting Helz");

        self.run().await
    }

    async fn run(self) -> Result<(), ProgramError> {
        let name = &self
            .config
            .expect("expecting to have a config when starting Helz")
            .name
            .expect("expecting name to be set")
            .to_owned();

        let mut handlers = futures::stream::futures_unordered::FuturesUnordered::new();
        for service in self.pollers.expect("expected some pollers, got none") {
            let name2 = name.to_owned();
            info!("starting pollers for '{}'", service.name);

            let handler = tokio::spawn(async move {
                service.start(name2).await;
            });
            handlers.push(handler);
        }

        let current_v = self.config_v.to_owned();
        let mut signals = self.signals.clone();

        loop {
            tokio::select! {
                // XXX: this cancels as soon as 1 returns, I think.. not really what I want
                _ = handlers.next() => {
                    debug!("pollers stopped");
                    break
                }
                _ = signals.changed() => {
                    debug!("got signal");
                    let signal = *signals.borrow_and_update();
                    debug!("got signal {} with current {}", signal, current_v);
                    if current_v != signal {
                        debug!("hup in run: {}", signal);
                        break
                    }
                }
            }
        }

        debug!("run finished, aborting handlers");
        for handle in handlers {
            handle.abort();
        }

        Ok(())
    }

    async fn configure(&mut self) -> Result<(), ProgramError> {
        debug!("configuring Helz");

        if self.config.is_some() {
            info!("reloading Helz configuration");
        }

        let config = parse_config(&self.config_path)?;
        self.config = Some(config.to_owned());

        for poller in config.polling {
            trace!("adding poller");
            let mut p = Polling::from(poller);
            if let Err(err) = p.configure_pollers().await {
                error!("failed to configure Helz: {:?}", err);
                return Err(ProgramError::ConfigError);
            }

            if let Some(existing_pollers) = &mut self.pollers {
                debug!("replacing existing pollers with new ones");
                let update_existing = existing_pollers
                    .iter_mut()
                    .any(|existing| existing.name == p.name);
                if update_existing {
                    trace!("updating existing poller for {}", p.name);
                    for existing in existing_pollers.iter_mut() {
                        if existing.name == p.name {
                            // TODO: consider if we should keep alert state
                            // I think it makes most sense to update
                            // "display" values of an alert rather than the values
                            // actually being checked, which means that values
                            // changed will only change how the alert is displayed
                            // - e.g. the name of it.
                            // If the checking value is changed, it's fair for it to trigger
                            // a new alert in my mind.
                            // The only time I think it makes sense to avoid triggering a new
                            // alert (if one is active) is if the config is exactly the same --
                            // in which case we can abort the configuration upgrade for that
                            // poller in specific.
                            debug!("re-configuring existing poller for {}", p.name);
                            *existing = p;
                            break;
                        }
                    }
                } else {
                    existing_pollers.push(p);
                }
            } else {
                if self.pollers.is_none() {
                    self.pollers = Some(vec![]);
                }
                if let Some(pollers) = &mut self.pollers {
                    pollers.push(p);
                }
            }
        }

        debug!("done configuring");

        Ok(())
    }
}

fn parse_config(path: &str) -> Result<Config, ProgramError> {
    debug!("parsing configuration");

    let config = match read_config_file(path.to_string()) {
        Ok(conf) => conf,
        Err(err) => {
            error!("failed to configure Helz: {:?}", err);
            return Err(ProgramError::ConfigError);
        }
    };

    Ok(config)
}

fn get_log_level(level: &str) -> LevelFilter {
    match level {
        "t" | "trace" => LevelFilter::Trace,
        "d" | "debug" | "v" | "verbose" => LevelFilter::Debug,
        "i" | "info" => LevelFilter::Info,
        "w" | "warn" | "warning" => LevelFilter::Warn,
        "e" | "error" => LevelFilter::Error,
        _ => {
            warn!("unparseable log level '{}', defaulting to Warn", level);
            LevelFilter::Warn
        }
    }
}

fn init_logger(cli: &Cli) {
    let mut logger_builder = env_logger::Builder::from_default_env();

    if let Some(str_log_level) = &cli.loglevel {
        let log_level = get_log_level(str_log_level);
        logger_builder.filter_level(log_level);
    }

    logger_builder.init();
}
