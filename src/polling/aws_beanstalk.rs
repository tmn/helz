/*! AWS Elastic Beanstalk Poller
*/

use async_trait::async_trait;
use aws_sdk_elasticbeanstalk::model::EnvironmentHealthStatus;
use aws_sdk_elasticbeanstalk::Client as BeanstalkClient;
use aws_smithy_types::{timeout, tristate::TriState};
use aws_types::region::Region;
use aws_types::sdk_config::{Builder as AwsSdkConfigBuilder, SdkConfig as AwsSdkConfig};
use serde::de::{self, Visitor};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::convert::From;
use std::ops::Deref;
use std::str::FromStr;

use crate::polling::config::{HelzCheck, HelzCheckError, HelzCheckResponse};
use crate::types::string_secret::Secret;

/// Configuration for the AWS Elastic Beanstalk poller
///
/// The values for the poller can be configured in TOML as follows:
/// ```
/// # use libhelz::polling::aws_beanstalk::AwsBeanstalkConfig;
/// # let conf: AwsBeanstalkConfig = toml::from_str(r#"
/// region = "us-east-1"
/// # "#).expect("example configuration should work");
/// ```
/// Or by specifying secret key, including a filter on environment names:
/// ```
/// # use libhelz::polling::aws_beanstalk::AwsBeanstalkConfig;
/// # let conf: AwsBeanstalkConfig = toml::from_str(r#"
/// region = "us-east-1"
/// environment_name = "my-application"
/// access_key_id = "foo"
/// access_key_secret = "bar"
/// # "#).expect("example configuration should work");
/// ```
///
/// Full example:
/// ```
/// # use libhelz::config::main::configure;
/// # let config = configure(r#"
/// [[polling]]
/// [polling.aws_beanstalk]
/// region = "us-east-1"
/// environment_name = "my-application"
/// [polling.notifier.stdout]
/// prefix = "beanstalk-error"
/// # "#)
/// # .expect("example configuration should work");
/// ```

#[derive(Clone, Debug, Deserialize, Serialize)]
/// Configuration for AWS Beanstalk polling.
pub struct AwsBeanstalkConfig {
    /// AWS Access Key ID, unless expected to be configured through IAM.
    pub access_key_id: Option<String>,
    /// AWS Access Key Secret, unless expected to be configured through IAM.
    pub access_key_secret: Option<Secret>,
    /// AWS region
    pub region: String,
    /// Filter for environment name
    pub environment_name: Option<String>,
    /// Filter for application name
    pub application_name: Option<String>,
    /// The timeout for a connection to AWS API.
    pub timeout: Option<std::time::Duration>,
    /// The timeout for waiting for a response from the AWS API.
    pub read_timeout: Option<std::time::Duration>,
    /// The maximum number of attempts made per request to the AWS API.
    pub max_attempts: Option<u32>,
    /// The names of health statuses to consider the beanstalk to be
    /// "healthy", as in not generate a health check error. Defaults to
    /// 'ok'.
    pub healthy_codes: Option<Vec<EnvironmentHealthStatusDeserializer>>,
}

#[derive(Debug)]
/// The runtime struct of the AWS Beanstalk poller.
pub struct AwsBeanstalk {
    environment_name: Option<String>,
    application_name: Option<String>,
    healthy_codes: Vec<EnvironmentHealthStatus>,
    client: Option<BeanstalkClient>,
    config: AwsBeanstalkConfig,
}

impl From<AwsBeanstalkConfig> for AwsBeanstalk {
    fn from(conf: AwsBeanstalkConfig) -> Self {
        let healthy_codes = conf
            .to_owned()
            .healthy_codes
            .unwrap_or_else(|| {
                vec![EnvironmentHealthStatusDeserializer(
                    EnvironmentHealthStatus::Ok,
                )]
            })
            .into_iter()
            .map(|code| code.0)
            .collect();

        let aws_conf = AwsBeanstalk::create_config(&conf);
        let client = aws_conf.map(AwsBeanstalk::create_client);

        Self {
            environment_name: conf.environment_name.to_owned(),
            application_name: conf.application_name.to_owned(),
            client,
            healthy_codes,
            config: conf,
        }
    }
}

#[async_trait]
impl HelzCheck for AwsBeanstalk {
    async fn configure(&mut self) -> Result<(), HelzCheckError> {
        if self.client.is_none() {
            let builder = AwsBeanstalk::configure_from_aws_sdk().await;
            let conf = AwsBeanstalk::create_sdk_client_config(&self.config, builder);
            self.client = Some(BeanstalkClient::new(&conf));
        }

        Ok(())
    }
    async fn run(self: &AwsBeanstalk) -> Result<HelzCheckResponse, HelzCheckError> {
        trace!("aws poll");

        let client = if let Some(client) = &self.client {
            client
        } else {
            return Err(HelzCheckError::AwsBsError(
                "no working client configured".to_string(),
            ));
        };

        let mut environments_request = client.describe_environments();

        if let Some(env) = &self.environment_name {
            trace!("adding environment name filter for '{}'", env);
            environments_request = environments_request.environment_names(env);
        }

        if let Some(app) = &self.application_name {
            trace!("adding application name filter for '{}'", app);
            environments_request = environments_request.application_name(app);
        }

        let environments_output = match environments_request.send().await {
            Ok(env) => env,
            Err(err) => {
                warn!("AWS API request failed");
                return Err(HelzCheckError::AwsBsError(err.to_string()));
            }
        };

        let environments = if let Some(environments) = environments_output.environments {
            environments
        } else {
            return Err(HelzCheckError::AwsBsError(
                "no environments in output".to_string(),
            ));
        };

        trace!("envs: {:?}", environments);

        // XXX Most other pollers only work for a single resource,
        // this one works for multiple at the same time.
        let mut errors = Vec::new();
        for env in environments {
            let health = env
                .health_status
                .to_owned()
                .unwrap_or(EnvironmentHealthStatus::UnknownValue);

            if !self.healthy_codes.contains(&health) {
                errors.push(env);
            }
        }

        if !errors.is_empty() {
            let msg = errors
                .into_iter()
                .map(|err| {
                    let env_name = err.environment_name.unwrap_or_else(|| "none".to_string());
                    let health = err
                        .health_status
                        .unwrap_or(EnvironmentHealthStatus::UnknownValue);
                    format!("{}:{:?}", env_name, health)
                })
                .collect::<Vec<String>>()
                .join("\n");

            return Err(HelzCheckError::AwsBsError(msg));
        }

        Ok(HelzCheckResponse { success: true })
    }
}

impl AwsBeanstalk {
    fn create_client(conf: AwsSdkConfig) -> BeanstalkClient {
        BeanstalkClient::new(&conf)
    }

    fn create_sdk_client_timeout_config(conf: &AwsBeanstalkConfig) -> timeout::Config {
        // XXX These defaults should probably align with (default) polling interval.
        // 3 second timeout x 3 retries = 9 seconds, default polling interval is 5 seconds - which
        //   means it will keep retrying into a new polling interval.
        //   Should we abort the future (poll())?
        let connect_timeout = conf
            .timeout
            .unwrap_or_else(|| std::time::Duration::from_secs(3));
        let read_timeout = conf
            .read_timeout
            .unwrap_or_else(|| std::time::Duration::from_secs(5));

        timeout::Config {
            api: timeout::Api::new()
                .with_call_timeout(TriState::Set(read_timeout))
                .with_call_attempt_timeout(TriState::Set(connect_timeout)),
            http: timeout::Http::new()
                .with_read_timeout(TriState::Set(read_timeout))
                .with_connect_timeout(TriState::Set(connect_timeout)),
            tcp: timeout::Tcp::new(),
        }
    }

    fn create_sdk_client_retry_config(
        conf: &AwsBeanstalkConfig,
    ) -> aws_smithy_types::retry::RetryConfig {
        let max_attempts = conf.max_attempts.unwrap_or(3);

        aws_config::RetryConfig::new().with_max_attempts(max_attempts)
    }

    fn create_sdk_client_config(
        conf: &AwsBeanstalkConfig,
        builder: AwsSdkConfigBuilder,
    ) -> AwsSdkConfig {
        let helz = &clap::crate_version!();
        builder
            .app_name(aws_types::app_name::AppName::new(helz.to_owned()).unwrap())
            .region(Region::new(conf.region.to_owned()))
            .retry_config(Self::create_sdk_client_retry_config(conf))
            .timeout_config(Self::create_sdk_client_timeout_config(conf))
            .sleep_impl(std::sync::Arc::new(
                aws_smithy_async::rt::sleep::TokioSleep::new(),
            ))
            .build()
    }

    fn create_config(conf: &AwsBeanstalkConfig) -> Option<AwsSdkConfig> {
        if conf.access_key_id.is_none() && conf.access_key_secret.is_none() {
            return None;
        }

        let builder = AwsBeanstalk::configure_from_helz(conf);

        Some(Self::create_sdk_client_config(conf, builder))
    }

    async fn configure_from_aws_sdk() -> AwsSdkConfigBuilder {
        let sdk_conf = aws_config::load_from_env().await;
        let credentials_provider = sdk_conf.credentials_provider().unwrap();

        let mut builder = AwsSdkConfig::builder();
        builder.set_credentials_provider(Some(credentials_provider.to_owned()));
        builder
    }

    fn configure_from_helz(conf: &AwsBeanstalkConfig) -> AwsSdkConfigBuilder {
        let helz = &clap::crate_version!();
        let cred = aws_types::credentials::Credentials::new(
            conf.access_key_id.to_owned().unwrap(),
            conf.access_key_secret.to_owned().unwrap().expose(),
            None,
            None,
            helz,
        );
        let provider = aws_types::credentials::SharedCredentialsProvider::new(cred);

        AwsSdkConfig::builder().credentials_provider(provider)
    }
}

#[derive(Clone, Debug)]
/// Used to proxy serializing/deserializing of AWS Environment Health status codes
/// for serializing and deserializing from string.
pub struct EnvironmentHealthStatusDeserializer(pub EnvironmentHealthStatus);

//impl std::fmt::Display for EnvironmentHealthStatusDeserializer {
impl Serialize for EnvironmentHealthStatusDeserializer {
    //fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = match self.0 {
            EnvironmentHealthStatus::Ok => "Ok",
            EnvironmentHealthStatus::Info => "Info",
            EnvironmentHealthStatus::NoData => "NoData",
            EnvironmentHealthStatus::Unknown(_) => "Unknown",
            EnvironmentHealthStatus::UnknownValue => "UnknownValue",
            EnvironmentHealthStatus::Pending => "Pending",
            EnvironmentHealthStatus::Warning => "Warning",
            EnvironmentHealthStatus::Degraded => "Degraded",
            EnvironmentHealthStatus::Severe => "Severe",
            EnvironmentHealthStatus::Suspended => "Suspended",
            _ => "NonMatched",
        };

        serializer.serialize_str(s)
    }
}

struct EnvironmentHealthStatusDeserializerVisitor;

impl<'de> Visitor<'de> for EnvironmentHealthStatusDeserializerVisitor {
    type Value = EnvironmentHealthStatusDeserializer;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("expecting a valid environment health status: []")
    }

    fn visit_string<E>(self, value: String) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        debug!(
            "string visitor for environment health status value:{}",
            value
        );
        EnvironmentHealthStatusDeserializer::from_str(&value).map_err(serde::de::Error::custom)
    }

    fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        debug!("&str visitor for environment health status value:{}", value);
        EnvironmentHealthStatusDeserializer::from_str(value).map_err(serde::de::Error::custom)
    }
}

impl FromStr for EnvironmentHealthStatusDeserializer {
    type Err = EnvironmentHealthStatusParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        debug!("parsing {} as environment health status", s);

        let status = match s.to_lowercase().as_str() {
            "ok" => EnvironmentHealthStatus::Ok,
            "info" => EnvironmentHealthStatus::Info,
            "nodata" => EnvironmentHealthStatus::NoData,
            "unknown" => EnvironmentHealthStatus::Unknown("".to_string()),
            "unknownvalue" => EnvironmentHealthStatus::UnknownValue,
            "pending" => EnvironmentHealthStatus::Pending,
            "warning" => EnvironmentHealthStatus::Warning,
            "degraded" => EnvironmentHealthStatus::Degraded,
            "severe" => EnvironmentHealthStatus::Severe,
            "suspended" => EnvironmentHealthStatus::Suspended,
            _ => {
                return Err(EnvironmentHealthStatusParseError::NoSuchStatus(
                    s.to_string(),
                ))
            }
        };

        Ok(EnvironmentHealthStatusDeserializer(status))
    }
}

#[derive(Debug)]
/// Failure modes for parsing Environment Health Status configurations
pub enum EnvironmentHealthStatusParseError {
    /// No status could be find for the specified string
    NoSuchStatus(String),
}

impl std::fmt::Display for EnvironmentHealthStatusParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            EnvironmentHealthStatusParseError::NoSuchStatus(s) => {
                write!(f, "could not find a status matching '{}'", s)
            }
        }
    }
}

impl<'de> Deserialize<'de> for EnvironmentHealthStatusDeserializer {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(EnvironmentHealthStatusDeserializerVisitor)
    }
}

impl Deref for EnvironmentHealthStatusDeserializer {
    type Target = aws_sdk_elasticbeanstalk::model::EnvironmentHealthStatus;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[cfg(test)]
mod tests {
    use serde::de::value::{Error as ValueError, StrDeserializer};
    use serde::de::IntoDeserializer;

    use super::*;

    #[test]
    fn error_if_empty_string() {
        let deserializer: StrDeserializer<ValueError> = "".into_deserializer();
        let err = deserializer
            .deserialize_str(EnvironmentHealthStatusDeserializerVisitor)
            .unwrap_err();
        assert_eq!(err.to_string(), "could not find a status matching ''");
    }

    #[test]
    fn expect_health_status_to_parse_case_insensitively() {
        let deserializer: StrDeserializer<ValueError> = "iNfo".into_deserializer();
        assert_eq!(
            deserializer
                .deserialize_str(EnvironmentHealthStatusDeserializerVisitor)
                .unwrap()
                .0,
            EnvironmentHealthStatus::Info
        );
    }
}
