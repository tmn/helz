/*! Useful types for the HTTP poller
 */

use serde::de::{self, Visitor};
use serde::{Deserialize, Deserializer, Serialize};
use std::str::FromStr;

#[derive(Clone, Debug, Serialize)]
/// Contains the [JSON Pointer](https://datatracker.ietf.org/doc/html/rfc6901)
/// for a check.
pub struct JsonPointer(pub String);

/// Error variants for a [JSONPointer](crate::polling::http::typex::JSONPointer).
pub enum ParseJsonPointerError {
    /// The JSON Pointer was invalid.
    InvalidPointer(String),
}

impl std::fmt::Display for ParseJsonPointerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ParseJsonPointerError::InvalidPointer(msg) => {
                write!(f, "json pointer parse error: {}", msg)
            }
        }
    }
}

impl FromStr for JsonPointer {
    type Err = ParseJsonPointerError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        debug!("parsing {} as json pointer", s);

        // Make sure first char is /
        // and that the string is non-empty
        match s.chars().next() {
            None => {
                return Err(ParseJsonPointerError::InvalidPointer(
                    "Pointer cannot be empty".to_owned(),
                ))
            }
            Some(c) => {
                if c != '/' {
                    return Err(ParseJsonPointerError::InvalidPointer(
                        "Pointer has to start with a '/'".to_owned(),
                    ));
                }
            }
        }

        Ok(JsonPointer(s.to_owned()))
    }
}

struct JsonPointerVisitor;

impl<'de> Visitor<'de> for JsonPointerVisitor {
    type Value = JsonPointer;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("expecting string starting with / and subsequent / for each nested key")
    }

    fn visit_string<E>(self, value: String) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        debug!("string visitor for json pointer value:{}", value);
        JsonPointer::from_str(&value).map_err(serde::de::Error::custom)
    }

    fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        debug!("&str visitor for json pointer value:{}", value);
        JsonPointer::from_str(value).map_err(serde::de::Error::custom)
    }
}

impl<'de> Deserialize<'de> for JsonPointer {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(JsonPointerVisitor)
    }
}

#[cfg(test)]
mod tests {
    use serde::de::value::{Error as ValueError, StrDeserializer};
    use serde::de::IntoDeserializer;

    use super::*;

    #[test]
    fn error_if_empty_string() {
        let deserializer: StrDeserializer<ValueError> = "".into_deserializer();
        let err = deserializer
            .deserialize_str(JsonPointerVisitor)
            .unwrap_err();
        assert_eq!(
            err.to_string(),
            "json pointer parse error: Pointer cannot be empty"
        );
    }
    #[test]
    fn error_if_not_starting_with_slash() {
        let deserializer: StrDeserializer<ValueError> = "json-pointer".into_deserializer();
        let err = deserializer
            .deserialize_str(JsonPointerVisitor)
            .unwrap_err();
        assert_eq!(
            err.to_string(),
            "json pointer parse error: Pointer has to start with a '/'"
        );
    }
    #[test]
    fn expect_pointer_to_work() {
        let deserializer: StrDeserializer<ValueError> = "/json/pointer".into_deserializer();
        assert_eq!(
            deserializer.deserialize_str(JsonPointerVisitor).unwrap().0,
            "/json/pointer"
        );
    }
}
