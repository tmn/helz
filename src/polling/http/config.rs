/*! Config for the HTTP poller
 */

use serde::{Deserialize, Serialize};
use url::Url;

use crate::polling::http::types::JsonPointer;

/// Configuration for the HTTP poller
///
/// The values can be configured in TOML as follows:
/// ```
/// # use libhelz::polling::http::config::HttpPollingConfig;
/// # let conf: HttpPollingConfig = toml::from_str(r#"
/// url = "http://localhost:1337/api/health"
/// timeout_s = 10
/// # "#).expect("example configuration should work");
/// ```
///
/// Full example including a JSON check
/// ```
/// # use libhelz::config::main::configure;
/// # let config = configure(r#"
/// [[polling]]
/// interval = "5s"
/// [polling.http]
/// url = "http://localhost:1234"
///
/// [polling.http.json]
/// pointer = "/path/to/val"
/// value = "success"
///
/// [polling.notifier]
/// interval = "5s"
/// [polling.notifier.stdout]
/// prefix = "error"
/// # "#).expect("example configuration should work");
/// ```
#[derive(Clone, Deserialize, Debug, Serialize)]
pub struct HttpPollingConfig {
    /// The URL to poll using the HTTP poller.
    pub url: Url,
    /// Timeout for HTTP request
    pub timeout_s: Option<u64>,
    /// Response code indicating a successful request.
    /// By default, a OK response (2xx) counts as successful.
    pub response_code: Option<u16>,
    /// Used to check the response body by parsing it as
    /// JSON and expecting a specific value in a specific field.
    pub json: Option<JsonCheck>,
    /// A custom user agent.
    pub user_agent: Option<String>,
}

/// Verify that the response from an HTTP request
/// matches the defined JSON value.
///
/// The values can be configured in TOML as follows:
/// ```
/// # use libhelz::polling::http::config::JsonCheck;
/// # let conf: JsonCheck = toml::from_str(r#"
/// pointer = "/path/to/value"
/// value = "success"
/// # "#).expect("example configuration should work");
/// ```
#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct JsonCheck {
    /// A [JSON Pointer](https://datatracker.ietf.org/doc/html/rfc6901)
    /// pointing to the value to check.
    pub pointer: JsonPointer,
    /// The value of the pointer to check.
    /// This can be any valid JSON value, so make
    /// sure to use match the type of the expected value.
    pub value: serde_json::Value,
}
