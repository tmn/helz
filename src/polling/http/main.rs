/*! HTTP polling program
 */

use async_trait::async_trait;
use reqwest::StatusCode;
use std::convert::From;
use url::Url;

use crate::polling::config::{HelzCheck, HelzCheckError, HelzCheckResponse};
use crate::polling::http::config::{HttpPollingConfig, JsonCheck};

/// The runtime struct for a HTTP poller.
pub struct HttpPolling {
    url: Url,
    timeout: std::time::Duration,
    response_code: Option<StatusCode>,
    json: Option<JsonCheck>,
    user_agent: String,
    client: reqwest::Client,
}

impl From<HttpPollingConfig> for HttpPolling {
    fn from(conf: HttpPollingConfig) -> Self {
        let response_code = conf.response_code.map(|response_code| {
            StatusCode::from_u16(response_code).expect("failed to parse response code as a number")
        });

        let user_agent = match conf.user_agent {
            Some(user_agent) => user_agent,
            None => "helz".to_owned(),
        };

        let timeout_s = conf.timeout_s.unwrap_or(10);
        let timeout = std::time::Duration::from_secs(timeout_s);

        let client = reqwest::Client::new();

        HttpPolling {
            url: conf.url,
            timeout,
            response_code,
            json: conf.json,
            client,
            user_agent,
        }
    }
}

#[async_trait]
impl HelzCheck for HttpPolling {
    async fn run(self: &HttpPolling) -> Result<HelzCheckResponse, HelzCheckError> {
        debug!("starting http check");

        let req = self
            .client
            .get(self.url.as_str())
            .timeout(self.timeout)
            .header("User-Agent", &self.user_agent);

        let resp = match req.send().await {
            Ok(resp) => resp,
            Err(err) => {
                debug!("error during http request: {}", err);
                return Err(HelzCheckError::HttpRequestError(format!(
                    "HTTP request error: {}",
                    err
                )));
            }
        };

        // @ToDo: match it to self.response_code
        if let Some(response_code) = self.response_code {
            if resp.status() != response_code {
                debug!(
                    "received non-matching status code: {} (expected {})",
                    resp.status(),
                    response_code
                );
                return Err(HelzCheckError::HttpRequestStatusCodeError(format!(
                    "received non-{} status code {}",
                    response_code,
                    resp.status()
                )));
            }
        } else if !resp.status().is_success() {
            return Err(HelzCheckError::HttpRequestStatusCodeError(format!(
                "received non-success status code {}",
                resp.status()
            )));
        }

        // We want to do some more checks before we return here maybe

        //
        if let Some(json_check) = &self.json {
            // Let's make sure the response is json

            match resp.json().await {
                Ok(body) => return self.check_json(body, json_check),
                Err(err) => {
                    debug!("failed with json pointer check: {}", err);
                    return Err(HelzCheckError::HttpNotJson("invalid json".to_owned()));
                }
            }
        }

        Ok(HelzCheckResponse { success: true })
    }
}

impl HttpPolling {
    fn check_json(
        &self,
        body: serde_json::Value,
        check: &JsonCheck,
    ) -> Result<HelzCheckResponse, HelzCheckError> {
        trace!("Checking for '{:?}' in body: {:?}", check.pointer.0, body);

        let res = traverse_json_object(body, &check.pointer.0);

        trace!("http json res:{:?}", res);

        match res {
            Ok(val) => {
                debug!("got val: {}", val);
                if val == check.value {
                    Ok(HelzCheckResponse { success: true })
                } else {
                    Err(HelzCheckError::HttpJsonInvalidData(format!(
                        "expected {:?}, got {:?}",
                        check.value, val
                    )))
                }
            }
            Err(err) => {
                error!(
                    "got an error while tryign to traverse json object: {:?}",
                    err
                );
                Err(HelzCheckError::HttpNotJson(
                    "error while trying to traverse json data".to_owned(),
                ))
            }
        }
    }
}

#[derive(Debug)]
/// The errors a JSON pointer check can return.
pub enum JsonPointerError {
    /// Returned if the path does not exist in the returned object.
    NoSuchPath,
}

fn traverse_json_object(
    obj: serde_json::Value,
    pointer: &str,
) -> Result<serde_json::Value, JsonPointerError> {
    let obj = match obj.pointer(pointer) {
        Some(val) => val,
        None => return Err(JsonPointerError::NoSuchPath),
    };

    Ok(obj.to_owned())
}
