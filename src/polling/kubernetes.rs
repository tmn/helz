/*! Kubernetes poller
 */
use async_trait::async_trait;
use k8s_openapi::api::apps::v1::Deployment;
use kube::{Api, Client};
use serde::{Deserialize, Serialize};
use std::convert::From;

use crate::config::traits::Example;
use crate::polling::config::{HelzCheck, HelzCheckError, HelzCheckResponse};

/// Configuration for the Kubernetes poller.
/// By default the poller will auto-configure itself based on the
/// KUBECONFIG environment variable or in-cluster environment variables
/// (if a ServiceAccount is provisioned and a token is available).
///
/// The values for what the poller will look for can be configured in TOML as follows:
/// ```
/// # use libhelz::polling::kubernetes::KubernetesConfig;
/// # let conf: KubernetesConfig = toml::from_str(r#"
/// namespace = "default"
/// deployment = "foo"
/// # "#).expect("example configuration should work");
/// ```
/// It might be possible to connect to a cluster using only
/// URL. It is preferred to infer the values from the env.
/// ```
/// # use libhelz::polling::kubernetes::KubernetesConfig;
/// # let conf: KubernetesConfig = toml::from_str(r#"
/// namespace = "default"
/// deployment = "foo"
/// [client_config]
/// cluster_url = "https://cluster.example.org"
/// timeout = "10s"
/// # "#).expect("example configuration should work");
/// ```
///
/// Full example:
/// ```
/// # use libhelz::config::main::configure;
/// # let config = configure(r#"
/// [[polling]]
/// [polling.kubernetes]
/// namespace = "default"
/// deployment = "foo"
/// [polling.notifier.stdout]
/// prefix = "kubernetes-error"
/// # "#)
/// # .expect("example configuration should work");
/// ```
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct KubernetesConfig {
    /// Provide static configuration for the Kubernetes API client.
    /// If left empty, Helz will attempt to configure the client dynamically
    /// by using e.g. KUBECONFIG.
    pub client_config: Option<KubeConfig>,
    /// Namespace to run poller in.
    pub namespace: String,
    /// Name of deployment to monitor.
    pub deployment: String,
}

/// Runtime struct for the Kubernetes poller.
pub struct Kubernetes {
    namespace: String,
    deployment: String,
    client: Option<Client>,
}

impl From<KubernetesConfig> for Kubernetes {
    fn from(conf: KubernetesConfig) -> Self {
        let client = match conf.client_config {
            Some(client_conf) => {
                let uri = http::uri::Uri::from(client_conf.cluster_url);
                let mut conf = kube::Config::new(uri);

                // TODO: implement this
                // conf.root_cert = client_conf.root_cert;

                conf.timeout = client_conf.timeout;
                conf.accept_invalid_certs = client_conf.accept_invalid_certs.unwrap_or(false);

                let builder = kube::client::ClientBuilder::try_from(conf)
                    .expect("failed to create kubernetes config from URL");
                Some(builder.build())
            }
            None => None,
        };

        Self {
            client,
            namespace: conf.namespace,
            deployment: conf.deployment,
        }
    }
}

#[async_trait]
impl HelzCheck for Kubernetes {
    async fn configure(&mut self) -> Result<(), HelzCheckError> {
        self.client = Some(Client::try_default().await.expect("a working client"));
        Ok(())
    }

    async fn run(&self) -> Result<HelzCheckResponse, HelzCheckError> {
        debug!("k8s check");
        let client = if let Some(client) = &self.client {
            client
        } else {
            return Err(HelzCheckError::KubernetesError(
                "no working client configured".to_string(),
            ));
        };

        let client: Api<Deployment> = Api::namespaced(client.to_owned(), &self.namespace);
        let deploy = client
            .get_status(&self.deployment)
            .await
            .expect("deployment");

        let conditions = &deploy
            .status
            .as_ref()
            .ok_or(HelzCheckError::KubernetesStatusError)?
            .conditions;

        if let Some(conditions) = conditions {
            let bad_conditions: Vec<&k8s_openapi::api::apps::v1::DeploymentCondition> = conditions
                .iter()
                .filter(|condition| condition.status == "False" || condition.status == "Unknown")
                .collect();

            if !bad_conditions.is_empty() {
                debug!("bad conditions found: {:?}", bad_conditions);

                let error_messages = bad_conditions
                    .iter()
                    .map(|condition| {
                        if let Some(message) = &condition.message {
                            message.to_owned()
                        } else {
                            "unknown error".to_string()
                        }
                    })
                    .collect::<Vec<String>>()
                    .join(",");

                return Err(HelzCheckError::KubernetesError(format!(
                    "ns={}, deployment={}, {}",
                    self.namespace, self.deployment, error_messages
                )));
            }
        }

        Ok(HelzCheckResponse { success: true })
    }
}

impl Example for KubernetesConfig {
    fn example() -> Self {
        Self {
            client_config: None,
            namespace: "default".to_string(),
            deployment: "foo".to_string(),
        }
    }
}

/// Configuration to pass when manually creating a kubernetes client.
// TODO: Figure out how to implement all kinds of manual auths
// other than the async ones reading from files/env/+++.
// Want to be able to set this statically 🤔
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct KubeConfig {
    /// URL the cluster.
    pub cluster_url: crate::types::uri::Uri,
    // TODO: Figure out how to implement this.
    // root_cert: Option<Vec<Vec<u8, alloc::alloc::Global>, alloc::alloc::Global>>,
    /// Timeout for cluster API calls.
    #[serde(default)]
    #[serde(with = "humantime_serde")]
    pub timeout: Option<std::time::Duration>,
    /// Whether to accept invalid certs or not.
    pub accept_invalid_certs: Option<bool>,
}
