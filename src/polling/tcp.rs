/*! TCP Poller
 */
use async_trait::async_trait;
use std::format;
use std::net::{Shutdown, TcpStream};

use serde::{Deserialize, Serialize};

use crate::config::traits::Example;
use crate::polling::config::{HelzCheck, HelzCheckError, HelzCheckResponse};

/// Configuration for the TCP poller
///
/// The values can be configured in TOML as follows:
/// ```
/// # use libhelz::polling::tcp::TcpPollingConfig;
/// # let conf: TcpPollingConfig = toml::from_str(r#"
/// address = "127.0.0.1"
/// port = 1337
/// # "#).expect("example configuration should work");
/// ```
///
/// Full example:
/// ```
/// # use libhelz::config::main::configure;
/// # let config = configure(r#"
/// [[polling]]
/// interval = "5s"
/// [polling.tcp]
/// address = "127.0.0.1"
/// port = 1337
/// [polling.notifier]
/// interval = "300s"
/// [polling.notifier.stdout]
/// prefix = "tcp"
/// # "#).expect("example configuration should work");
/// ```
#[derive(Clone, Deserialize, Debug, Serialize)]
pub struct TcpPollingConfig {
    /// The address to try to open a TCP connection towards
    pub address: String,
    /// The port to try to open a TCP connection towards
    pub port: u16,
}

#[async_trait]
impl HelzCheck for TcpPollingConfig {
    async fn run(self: &TcpPollingConfig) -> Result<HelzCheckResponse, HelzCheckError> {
        debug!("starting tcp check");

        let conn = TcpStream::connect(format!("{}:{}", self.address, self.port));

        let stream = match conn {
            Ok(s) => s,
            Err(err) => {
                debug!("tcp connection failed ({})", err);
                return Err(HelzCheckError::TcpPollingFailure(format!(
                    "TCP error for {}:{} ({:?})",
                    self.address, self.port, err
                )));
            }
        };
        let shutdown = stream.shutdown(Shutdown::Both);

        match shutdown {
            Ok(_) => (),
            Err(err) => println!("error during tcp shutdown ({})", err),
        };

        debug!("done");

        Ok(HelzCheckResponse { success: true })
    }
}

impl Example for TcpPollingConfig {
    fn example() -> Self {
        Self {
            address: "127.0.0.1".to_owned(),
            port: 5432,
        }
    }
}
