/*! A String type to prevent accidental exposure of secrets
 */

use serde::{Deserialize, Serialize};

#[derive(Clone, Deserialize)]
/// The Secret type is a wrapper around a String
/// but is supposed to be used where exposing secrets
/// in i.e. a log is unwanted. The Secret has to be
/// explicitly `.expose()`d to return the underlying value,
/// so that a `log!("secret: {}", Secret("foo"))` only returns
/// "redacted".
pub struct Secret(pub String);

impl std::fmt::Display for Secret {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<redacted>")
    }
}
impl std::fmt::Debug for Secret {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<redacted>")
    }
}

impl Serialize for Secret {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str("<redacted>")
    }
}

impl Secret {
    /// Calling this function returns the underlying
    /// String of the Secret.
    pub fn expose(self) -> String {
        self.0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_string_format_redacts() {
        let secret: Secret = Secret("foo".to_string());
        assert_eq!(format!("{}", secret), "<redacted>");
    }

    #[test]
    fn test_expose_exposes() {
        let secret: Secret = Secret("foo".to_string());
        assert_eq!(secret.expose(), "foo");
    }

    #[test]
    fn test_serialize_redacts() {
        let secret: Secret = Secret("foo".to_string());
        assert_eq!(
            toml::to_string(&secret).unwrap(),
            toml::to_string("<redacted>").unwrap()
        );
    }
}
