/*! Serializable and deserializable URI type
*/

use serde::{Deserialize, Serialize};

/// A (de)serializable Uri struct.
// TODO: Consider implementing deserialize manually
// so that we can fail on invalid URLs and can drop
// the valid() func.
#[derive(Clone, Deserialize, Serialize)]
pub struct Uri(pub String);

impl std::fmt::Display for Uri {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::fmt::Debug for Uri {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<Uri> for http::uri::Uri {
    fn from(uri: Uri) -> Self {
        uri.0
            .parse::<http::uri::Uri>()
            .expect("failed to parse url")
    }
}

/// The error variants of Uri if it fails to parse.
pub enum UriError {
    /// The generic error if a URI fails to parse.
    InvalidUri,
}

impl Uri {
    /// Verify if a configured Uri is valid.
    pub fn valid(self) -> Result<(), UriError> {
        match self.0.parse::<http::uri::Uri>() {
            Ok(_) => Ok(()),
            Err(_) => Err(UriError::InvalidUri),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn string_produces_uri() {
        let uri: Uri = Uri("https://example.org".to_string());
        assert_eq!(format!("{}", uri), "https://example.org");
    }

    #[test]
    #[should_panic]
    fn invalid_uri() {
        let crate_uri: Uri = Uri("\nf\no\no".to_string());
        let _ = http::uri::Uri::from(crate_uri.to_owned());
    }
}
