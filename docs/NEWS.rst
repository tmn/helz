Helz v0.13.0
============

New features:

* Handle reloading configuration by sending Helz a SIGHUP
* Helz knows who it is (where it runs), so it can report that as well. Defaults to hostname, but can be configured
  explicitly as a top-level configuration parameter. No more "this alert was triggered, but I have no idea from where!"

Bug fixes:

* Showing/checking configuration now correctly redacts secrets.

Maintenance:

* Upgraded a few dependencies.
* Adds a test for serializing and deserializing configuration, which should catch
  out-of-order TOML keys.

Helz v0.12.0
============

feat: Add Kubernetes Deployment poller.

Helz v0.11.0
============

feat: Add OpsGenie notifier.

Helz v0.10.2
============

fix: Add a sleep implementation for the aws beanstalk poller.

Helz v0.10.1
============

fix: Don't panic on unset values in aws_beanstalk `access_key_*`.

Helz v0.10.0
============

feat: Support default AWS credential provider chain. This allows
to *not* specify AWS client key id & AWS secret key, but rather
assume roles through default supported AWS chain such as EC2 node
roles or STS/WebIdentity.

Helz v0.9.5
===========

fix: Don't send an OK notification if we never sent a notification.

Helz v0.9.4
===========

fix: Make the timeout-feature actually work.

Helz v0.9.3
===========

fix: Make sure ca-certificates is installed in the release image.

Helz v0.9.2
===========

Helz now releases docker images.

Helz v0.9.1
===========

Allows filtering on AWS Beanstalk environment health statuses,
for example treating the "info" level as a healthy status,
therefore not triggering a health alert.

Helz v0.9.0
===========

Implements the AWS Beanstalk poller and the Slack notifier.

Also adds improvements to documentation and upgrades the project
to Rust 2021 edition.

Helz v0.8.0
===========

Fixes a bug in which only the first configured poller would run.

Runs all pollers before notifying, so that the first notification
will have context from all of the pollers, if more than one poller
returned an error.

Helz v0.7.0
===========

Add human-readable 'interval' configurations to take over for the
``interval_s`` and ``timeout_s`` options. Both variants will work
for a transition period, but the ``_s`` will be removed in the future.

Helz v0.6.0
===========

Add optional ``require_enabled`` field to systemd unit poller.

Enabling this will disable notifying about units that are deactivated
or failed while it is disabled in systemd(1). This can be useful in
the case of primary/secondary nodes where the secondary node is cold
and offline, but as soon as it is promoted, health checks should run
for it. It is not needed to restart Helz for this change to take effect.

The default value for this option is disabled.

Helz v0.5.2
===========

Make sure the newly added manpage is distributed in the packages.

Helz v0.5.1
===========

Add a manpage with more content.

Helz v0.5.0
===========

Add documentation for all pollers and notifiers.

Helz v0.4.1
===========

Allow the systemd unit to use AF_UNIX addresses, which is required
for calling systemd(1) programs.

Helz v0.4.0
===========

Adds a systemd requirement for network-online.target.
While it is not mandatory, most reporting would require it.

Helz v0.3.6
===========

Generalises error and ok message generation so they'll be the same for rich
messages such as Discord and Webex. Adds release notes to release automagically,
as well as as changelogs to packages.

Helz v0.3.5
===========

Redacts configuration secrets in log output. Also fixes some systemd service
warnings.

Helz v0.3.4
===========

Makes --version flag give the correct version instead of 0.1.
Also the first release to ship .deb and .rpm packages.

Helz v0.3.3
===========

Released at: 2021.07.09

Fix a bug where a systemd check could panic Helz if it responded with a
non-success exit code.
Note: Examples of where `systemd show unit --property=ActiveState`
could return a non-success exit code are welcome!

Helz v0.3.2
===========

Released at: 2021.07.08

This release contains mostly internal bug fixes and automation of releases.

Helz v0.3.1
===========

Released at: 2021.07.05

Reverts the TCP requiring a valid IP address for its 'address' parameter, as
'localhost' would no longer be a valid TCP address to poll.

Helz v0.3.0
===========

Released at: 2021.07.05

Add HTTP poller and make pollers asynchronous. Makes TCP poller require a valid IP address
for its 'address' configuration parameter.

Helz v0.2.0
===========

Released at: 2021.06.30

Add webex and discord notification support. Rework the internal application structure
so that it uses Tokio instead of async_std, because serenity-rs uses tokio.

Helz v0.1.0
===========

Released at: 2021.06.23

Initial release. Basic TCP health check, stdout and email notifiers.
