FROM rust:latest AS builder

RUN mkdir /app
WORKDIR /app

# Systemd lib
WORKDIR /app/systemd-lib
COPY Cargo.lock .
COPY Cargo.toml .
COPY systemd-lib/ .
RUN cargo fetch --locked

# Helz
WORKDIR /app
COPY Cargo.lock .
COPY Cargo.toml .
RUN cargo fetch --locked

COPY . .
RUN cargo build --release --locked

# Release image
FROM debian:latest

RUN apt update -y
RUN apt install -y openssl ca-certificates

COPY --from=builder /app/target/release/helz /bin/helz

CMD ["/bin/helz", "--config", "/etc/helz.toml"]
