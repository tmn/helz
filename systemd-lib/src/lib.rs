#[macro_use]
extern crate log;

use std::process::Command;
use std::str::FromStr;

#[cfg(test)]
mod tests {

    use crate::Systemd;

    #[test]
    fn systemd_command_works() {
        let systemd_lib = Systemd::new("./bin/systemd-active.sh");

        let active_state = crate::UnitState::Active(crate::ActiveState::Unknown);

        systemd_lib
            .get_state("service", &active_state)
            .expect("expecting the basic systemctl command to work");
    }

    #[test]
    fn parse_systemctl_active_state_works() {
        let mut systemd_lib = Systemd::default();
        systemd_lib.bin = |_: &str, property: &str, state: &str| -> Result<String, crate::ParseError> { Ok(format!("{}={}", property, state)) };

        let expected_state = crate::UnitState::Active(crate::ActiveState::Unknown);

        let property = expected_state.to_systemd_property();
        let state = "active";
        let systemd_resp = format!("{}={}", property, state);


        let state = systemd_lib
            .parse_property(&systemd_resp, property)
            .expect("failed to parse property string");

        assert_eq!(crate::UnitState::Active(crate::ActiveState::Active), systemd_lib.into_state(&expected_state, &state).unwrap());
    }

    #[test]
    fn systemctl_get_enabled_state() {
        let mut systemd_lib = Systemd::default();
        systemd_lib.bin = |_: &str, property: &str, state: &str| -> Result<String, crate::ParseError> { Ok(format!("{}={}", property, state)) };

        let expected_state = crate::UnitState::Enabled(crate::EnabledState::Unknown);

        let property = expected_state.to_systemd_property();
        let state = "enabled";
        let systemd_resp = format!("{}={}", property, state);

        let state = systemd_lib
            .parse_property(&systemd_resp, property)
            .expect("to be able to identify the enabled-state of the unit");

        assert_eq!(crate::UnitState::Enabled(crate::EnabledState::Enabled), systemd_lib.into_state(&expected_state, &state).expect(&format!("expected parsing '{}' into state to work", state)));
    }

    #[test]
    fn systemctl_unhandled_state_parser_returns_unknown() {
        use std::str::FromStr;

        let state = "unbeknownst-to-humanity";

        match crate::ActiveState::from_str(state) {
            Ok(_) => assert!(false, "expected parse to return an error, but it succeeded"),
            Err(err) => match err {
                crate::ParseError::UnknownState => assert!(true, "expected parser to error with UnknownState, but got {:?}", err),
                _ => assert!(false, "expected parser to error with UnknownState, but got {:?}", err),
            },
        };
    }


    #[test]
    fn systemctl_unhandled_state_doesnt_panic() {
        let mut systemd_lib = Systemd::default();
        systemd_lib.bin = |_: &str, property: &str, state: &str| -> Result<String, crate::ParseError> { Ok(format!("{}={}", property, state)) };

        let expected_state = crate::UnitState::Active(crate::ActiveState::Unknown);

        let property = expected_state.to_systemd_property();
        let state = "unbeknownst-to-humanity";
        let systemd_resp = format!("{}={}", property, state);

        match systemd_lib
            .parse_property(&systemd_resp, property) {
            Ok(state) => match systemd_lib.into_state(&expected_state, &state) {
                Err(err) => match err {
                    crate::ParseError::UnknownState => assert!(true, "expected parser to error with UnknownState, but got {:?}", err),
                    _ => assert!(false, "expected parser to error with UnknownState, but got {:?}", err),
                },
                Ok(parsed) => assert!(false, "expected parser to error with UnknownState, but it parsed successfully as {:?}", parsed),
            },
            Err(err) => assert!(false, "expected parser to error with UnknownState, but got {:?}", err),
        };
    }
}

pub struct Systemd {
    pub path: Path,
    bin: fn (path: &str, service: &str, state: &str) -> Result<String, ParseError>,
}

type Path = String;

impl std::default::Default for Systemd {
    fn default() -> Self {
        Self {
            path: "systemctl".to_owned(),
            bin: |path, service, state| { run_systemd_command(path, service, state) },
        }
    }
}

// Fetched from https://www.freedesktop.org/software/systemd/man/org.freedesktop.systemd1.html#Properties1
#[derive(Debug, PartialEq)]
pub enum ActiveState {
    Active,
    Inactive,
    Failed,
    Deactivating,
    Activating,
    Unknown,
}

// Fetched from https://www.freedesktop.org/software/systemd/man/org.freedesktop.systemd1.html#Properties1
#[derive(Debug, PartialEq)]
pub enum EnabledState {
    Enabled,
    EnabledRuntime,
    Linked,
    LinkedRuntime,
    Masked,
    MaskedRuntime,
    Static,
    Disabled,
    Invalid,
    Unknown,
}

impl FromStr for ActiveState {
    type Err = ParseError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "active" => Ok(Self::Active),
            "inactive" => Ok(Self::Inactive),
            "failed" => Ok(Self::Failed),
            "activating" => Ok(Self::Activating),
            "deactivating" => Ok(Self::Deactivating),
            _ => Err(ParseError::UnknownState),
        }
    }
}

impl FromStr for EnabledState {
    type Err = ParseError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "enabled" => Ok(Self::Enabled),
            "enabled-runtime" => Ok(Self::EnabledRuntime),
            "linked" => Ok(Self::Linked),
            "linked-runtime" => Ok(Self::LinkedRuntime),
            "masked" => Ok(Self::Masked),
            "masked-runtime" => Ok(Self::MaskedRuntime),
            "static" => Ok(Self::Static),
            "invalid" => Ok(Self::Invalid),
            "disabled" => Ok(Self::Disabled),
            _ => Err(ParseError::UnknownState),
        }
    }
}

fn run_systemd_command(systemd_bin: &str, service: &str, property: &str) -> Result<String, ParseError> {
        // Systemctl command:
        // systemctl show --property=PROPERTY
        // Returns:
        // PROPERTY=value

        let res = Command::new(systemd_bin)
            .args(&["show", &format!("--property={}", property), &service])
            .output()
            .expect("failed to execute systemctl show");

        trace!("{:?}", res);

        if !res.status.success() {
            return Err(ParseError::NonSuccessExit);
        }

        Ok(String::from_utf8_lossy(&res.stdout).to_string())
}

impl Systemd {

    fn parse_property(&self, systemd_response: &str, property: &str) -> Result<String, ParseError> {
        if systemd_response.len() < property.len() + 1 {
            return Err(ParseError::InvalidResponseData);
        }

        let value = &systemd_response[property.len() + 1..systemd_response.len()];
        Ok(value.trim().to_owned())
    }

    fn into_state(&self, wanted_state: &UnitState, state: &str) -> Result<UnitState, ParseError> {
        match wanted_state {
            UnitState::Active(_) => Ok(UnitState::Active(ActiveState::from_str(&state)?)),
            UnitState::Enabled(_) => Ok(UnitState::Enabled(EnabledState::from_str(&state)?)),
        }
    }

    pub fn get_state(&self, service: &str, unit_state: &UnitState) -> Result<UnitState, ParseError> {
        let property = unit_state.to_systemd_property();
        let systemd_response = (self.bin)(&self.path, service, property)?;
        let state = self.parse_property(&systemd_response, &property)?;
        self.into_state(unit_state, &state)
    }

    pub fn new(path: &str) -> Self {
        let mut lib = Self::default();
        lib.path = path.to_owned();
        lib
    }
}


#[derive(Debug, PartialEq)]
pub enum UnitState {
    /// Active contains the result of the active state as given by systemd(1)
    /// when issuing the `show --property=ActiveState` command.
    Active(ActiveState),
    /// Enabled contains the result of the enabled state as given by systemd(1)
    /// when issuing the `show --property=UnitFileState` command.
    Enabled(EnabledState),
}

impl UnitState {
    pub fn to_systemd_property(&self) -> &str {
        match self {
            UnitState::Active(_) => "ActiveState",
            UnitState::Enabled(_) => "UnitFileState",
        }
    }
}

#[derive(Debug)]
pub enum ParseError {
    NonSuccessExit,
    InvalidResponseData,
    UnknownState,
}
