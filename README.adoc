= Helz
:toc: preamble

image:https://img.shields.io/gitlab/v/release/helz/helz[link="https://gitlab.com/helz/helz/-/releases"]
image:https://gitlab.com/helz/helz/badges/main/pipeline.svg[link="https://gitlab.com/helz/helz/-/pipelines/latest?scope=branches"]
image:https://img.shields.io/badge/docs-passing-success[link="https://helz.gitlab.io/helz"]

Helz is a health checking program.

Helz queries services you wish by polling them using programmed checks.
The pollers are implemented in `src/polling/` and documented
https://helz.gitlab.io/helz/libhelz/polling/index.html[here].

To notify about state changes you may use notifiers. They are implemented in `src/notifiers/` and
documented https://helz.gitlab.io/helz/libhelz/notifying/index.html[here].

== Installation

Download a fresh release from the https://gitlab.com/helz/helz/-/releases[releases page].
All releases are built as a standalone linux binary, deb and rpm packages and docker images.

== Usage

Helz is configured using a configuration file in TOML format. For each of the pollers
and notifiers, there are full examples available. For helpfulness' sake, here is an example configuration
which will check that the service on the network address `127.0.0.1` and port `3306` is accepting TCP connections.

.Helz configuration example
[source,toml]
----
[[polling]]
name = "HelzCheck"
interval = "5s"

[polling.tcp]
address = "127.0.0.1"
port = 5432

[polling.systemd]
unit = "helz.service"
require_enabled = false

[polling.notifier]
timeout = "0s"
interval = "5m"

[polling.notifier.stdout]
prefix = ""

[polling.notifier.discord]
webhook_id = 0
api_token = "foo"

[polling.notifier.webex]
api_token = "bar"
room_id = "foo"

----


The `[[ polling ]]` header denotes a list of such an attribute, which means that to add multiple different pollers
(e.g. for different services) you can add the `[[ polling ]]` line multiple times.

== Project Moved

Due to how GitLab https://about.gitlab.com/solutions/open-source/join/[changed its policy regarding Open Source projects and receiving GitLab Ultimate benefits],
the project had to be moved to a namespace which would satsify its new requirements. This requires updating URLs
to this repository, such as for git, docs, releases/downloads and containers. This sadly broke all container
image links, but they are all still present - just update the URL from `gitlab.com/sklirg/helz` to `gitlab.com/helz/helz`.

Some of the other URLs will continue working and redirect correctly.

== Name

Helz is just a wordplay on the Norwegian word for 'health', _helse_. Sorry.

